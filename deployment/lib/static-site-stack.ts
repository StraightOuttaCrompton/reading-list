import * as cdk from "@aws-cdk/core";
import StaticSite from "aws-cdk-static-site";

interface IProps extends cdk.StackProps {
    environment?: "production" | "development";
    siteSubDomain?: string;
    domainName: string;
    siteAssetsPath: string;
}

export default class StaticSiteStack extends cdk.Stack {
    constructor(scope: cdk.App, id: string, props: IProps) {
        const {
            siteSubDomain,
            domainName,
            siteAssetsPath,
            environment = "development",
            ...rest
        } = props;

        super(scope, id, rest);

        if (environment === "production") {
            new StaticSite(this, id, {
                siteSubDomain,
                domainName,
                siteAssetsPath,
                constructConfig: {
                    useRoute53: true,
                    useCloudfront: true, // needed for https
                    useDeletableBucket: false,
                },
            });
        } else if (environment === "development") {
            new StaticSite(this, id, {
                siteSubDomain,
                domainName,
                siteAssetsPath,
                constructConfig: {
                    useRoute53: true,
                    useCloudfront: false,
                    useDeletableBucket: true,
                },
            });
        }
    }
}
