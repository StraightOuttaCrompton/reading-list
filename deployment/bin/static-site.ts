#!/usr/bin/env node
import * as cdk from "@aws-cdk/core";
import StaticSiteStack from "../lib/static-site-stack";

const {
    SITE_SUB_DOMAIN,
    DOMAIN_NAME,
    SITE_ASSETS_PATH,
    AWS_DEFAULT_REGION,
    AWS_DEFAULT_ACCOUNT,
    ENVIRONMENT,
} = process.env;

if (typeof DOMAIN_NAME === "undefined")
    throw new Error("DOMAIN_NAME environment variable must be defined");
if (typeof SITE_ASSETS_PATH === "undefined")
    throw new Error("SITE_ASSETS_PATH environment variable must be defined");

if (
    typeof ENVIRONMENT !== "undefined" &&
    ENVIRONMENT !== "production" &&
    ENVIRONMENT !== "development"
)
    throw new Error(
        "ENVIRONMENT environment variable must be either 'production' or 'development'"
    );

const id =
    `${typeof SITE_SUB_DOMAIN === "undefined" ? "" : SITE_SUB_DOMAIN + "."}${DOMAIN_NAME}`.replace(
        /\./g,
        "-"
    ) + "-SiteStack";

const app = new cdk.App();
new StaticSiteStack(app, id, {
    siteSubDomain: SITE_SUB_DOMAIN,
    domainName: DOMAIN_NAME,
    siteAssetsPath: SITE_ASSETS_PATH,
    environment: ENVIRONMENT,
    env: {
        account: AWS_DEFAULT_ACCOUNT,
        region: AWS_DEFAULT_REGION,
    },
});
