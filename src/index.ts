import "fontsource-roboto";

if ("serviceWorker" in navigator) {
    const sw = "serviceWorker.js"; // it is needed because parcel will not recognize this as a file and not precess in its manner

    navigator.serviceWorker
        .register(sw)
        .then((registration) => {
            registration.onupdatefound = () => {
                const installingWorker = registration.installing;
                if (installingWorker == null) {
                    return;
                }
                installingWorker.onstatechange = () => {
                    if (installingWorker.state === "installed") {
                        if (navigator.serviceWorker.controller) {
                            console.log(
                                "New content is available and will be used when all " +
                                    "tabs for this page are closed. See https://bit.ly/CRA-PWA."
                            );
                        } else {
                            console.log("Content is cached for offline use.");
                        }
                    }
                };
            };
        })
        .catch((error) => {
            console.error("Error during service worker registration:", error);
        });
} else {
    console.log("service worker is not supported.");
}

const { CI_COMMIT_TIMESTAMP } = process.env;

document.querySelectorAll(".last-updated").forEach((element) => {
    if (typeof CI_COMMIT_TIMESTAMP === "undefined") return;

    const lastUpdatedDate = new Date(CI_COMMIT_TIMESTAMP);

    element.innerHTML = `<span>Last Updated:</span><em>${lastUpdatedDate.toDateString()}</em>`;
});
