# ![books](favicon-32x32.png) [SOCs](https://straightouttacrompton.co.uk/) reading list

This is a list of books I have read in recent years. If you have any recomendations, please feel free to [contact me](https://straightouttacrompton.co.uk/#contact)!

## 2022

- **Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy** - _Cathy O'Neil_

## 2021

-   **The Hitchhiker's Guide to the GalaxyThe Hitchhiker's Guide to the Galaxy** - _Douglas Adams_
-   **What Do You Care What Other People Think?: Further Adventures of a Curious Character** - _Richard Feynman_
-   **Flat Earth News: An Award-Winning Reporter Exposes Falsehood, Distortion and Propaganda in the Global Media** - _Nick Davies_
-   **Lord of the Flies** - _William Golding_
-   **A Clockwork Orange** - _Anthony Burgess_
-   **You Are Not A Gadget: A Manifesto** - _Jaron Lanier_
-   **The Testaments** - _Margaret Atwood_

## 2020

-   **A Book of Bees: And How to Keep Them** - _Sue Hubbell_
-   **The Handmaid's Tale** - _Margaret Atwood_
-   **No Logo** - _Naomi Klein_
-   **The Wall** - _John Lanchester_
-   **Prisoners of Geography: Ten Maps That Tell You Everything You Need to Know About Global Politics** - _Tim Marshall_
-   **The Heretics: Adventures with the Enemies of Science** - _Will Storr_
-   **The Circle** - _Dave Eggers_
-   **Private Island: Why Britain Now Belongs to Someone Else** - _James Meek_
-   **Mythos: The Greek Myths Retold** - _Stephen Fry_\*
-   **A Song of Ice and Fire** - _George R. R. Martin_\* _(Read by Roy Dotrice)_

## 2019

-   **Copy, Rip, Burn: The Politics of Copyleft and Open Source** - _David M. Berry_
-   **This is Going to Hurt: Secret Diaries of a Junior Doctor** - _Adam Kay_
-   **The Art of War** - _Sun Tzu_
-   **The Book of Lord Shang** - _Shang Yang_
-   **Burmese Days** - _George Orwell_
-   **Why I Write** - _George Orwell_
-   **A Brief History of the Future: The Origins of the Internet** - _John Naughton_

## 2018

-   **Fahrenheit 451** - _Ray Bradbury_
-   **Kingpin: How One Hacker Took Over the Billion-Dollar Cybercrime Underground** - _Kevin Poulsen_
-   **Death Note** - _Tsugumi Ohba, Takeshi Obata_
-   **Trust Me, I'm Lying: Confessions of a Media Manipulator** - _Ryan Holiday_
-   **SuperFreakonomics: Global Cooling, Patriotic Prostitutes, and Why Suicide Bombers Should Buy Life Insurance** - _Steven D. Levitt, Stephen J. Dubner_
-   **Freakonomics: A Rogue Economist Explores the Hidden Side of Everything** - _Steven D. Levitt, Stephen J. Dubner_
-   **The Code Book: The Science of Secrecy from Ancient Egypt to Quantum Cryptography** - _Simon Singh_
-   **Fermat's Last Theorem** - _Simon Singh_
-   **The Man Who Mistook His Wife for a Hat and Other Clinical Tales** - _Oliver Sacks_
-   **The Music of the Primes: Why an Unsolved Problem in Mathematics Matters** - _Marcus du Sautoy_
-   **Brave New World** - _Aldous Huxley_

## 2017

-   **The Hut Six Story: Breaking the Enigma Codes** - _Gordon Welchman_
-   **The War of the Worlds** - _H. G. Wells_
-   **The Curious Incident of the Dog in the Night-Time** - _Mark Haddon_
-   **Frankenstein** - _Mary Shelley_
-   **Manufacturing Consent: The Political Economy of the Mass Media** - _Edward S. Herman, Noam Chomsky_
-   **Sapiens: A Brief History of Humankind** - _Yuval Noah Harari_
-   **Wiseguy: Life in a Mafia Family** - _Nicholas Pileggi_
-   **The Diary of a Young Girl** - _Anne Frank_

## 2016

-   **I Am Legend** - _Richard Matheson_
-   **The Beach** - _Alex Garland_
-   **World War Z: An Oral History of the Zombie War** - _Max Brooks_
-   **1984** - _George Orwell_
-   **Surely You're Joking, Mr. Feynman!** - _Richard Feynman_
-   **The Call of the Weird: Travels in American Subcultures** - _Louis Theroux_
-   **Creativity, Inc.: Overcoming the Unseen Forces That Stand in the Way of True Inspiration** - _Ed Catmull, Amy Wallace_
-   **What We Cannot Know: Explorations at the Edge of Knowledge** - _Marcus du Sautoy_

## Pre 2016

-   **Animal Farm** - _George Orwell_
-   **Of Mice and Men** - _John Steinbeck_
-   **The Day of the Triffids** - _John Wyndham_
-   **The Hobbit** - _J. R. R. Tolkien_
-   **Quantum Theory Cannot Hurt You** - _Marcus Chown_
-   **The Quantum Universe: Everything That Can Happen Does Happen** - _Brian Cox, Jeff Forshaw_
    **Why Does E=mc²? (And Why Should We Care?)** - _Brian Cox, Jeff Forshaw_
-   **Universal: A Guide to the Cosmos** - _Brian Cox, Jeff Forshaw_
-   **A Brief History of Time: From the Big Bang to Black Holes** - _Stephen Hawking_
-   **Between a Rock and a Hard Place** - _Aron Ralston_

_\*Audiobook (totally counts)_
