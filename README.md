# Reading list

Source code for a site containing a list of books I have read in recent years. The site is hosted at https://readinglist.straightouttacrompton.co.uk/.

List is maintained [here](./src/ReadingList.md).
